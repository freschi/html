import at.femoweb.html.Document;
import at.femoweb.html.HTMLReader;
import at.femoweb.html.Tag;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.*;

import java.net.URL;

/**
 * Created by felix on 10/27/14.
 */
public class DocumentTest {

    @Test
    @Ignore
    public void testSomething() throws Exception {
        Document something = HTMLReader.open(new URL("http://www.codedread.com/testbed/pure-html.html"));
        assertEquals("Doctype", "html", something.doctype());
        Tag body = something.content().select().name("body").first();
        Tag link = body.get(3).select().name("a").first();
        assertEquals("Link Text", "Jeff Schiller", link.text());
    }
}
