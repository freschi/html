package at.femoweb.html;

/**
 * Created by felix on 10/22/14.
 */
public enum TagType {
    FULL, HALF, SHORT
}
