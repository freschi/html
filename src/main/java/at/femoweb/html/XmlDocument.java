package at.femoweb.html;

/**
 * Created by felix on 2/18/15.
 */
public class XmlDocument extends Document {

    public XmlDocument(String rootName) {
        super("xml");
        body(new Tag(rootName));
    }

    @Override
    public Tag head() {
        return null;
    }

    @Override
    public void head(Tag head) {

    }

    public Tag root() {
        return body();
    }

    public void root(Tag root) {
        body(root);
    }

    @Override
    public String render() {
        String ret = "";
        ret += "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        return ret + content().render();
    }
}
