package at.femoweb.html;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by felix on 10/22/14.
 */
public class Selection extends ArrayList<Tag> {

    private Selection (ArrayList<Tag> tags) {
        this.addAll(tags);
    }

    public Selection name(String name) {
        Selection ret = new Selection(this);
        Iterator<Tag> iterator = ret.iterator();
        while (iterator.hasNext()) {
            Tag tag = iterator.next();
            if(tag.name() != null) {
                if (!tag.name().equalsIgnoreCase(name)) {
                    iterator.remove();
                }
            } else
                iterator.remove();
        }
        return ret;
    }

    public Selection attribute(String name, String value) {
        Selection ret = new Selection(this);
        Iterator<Tag> iterator = ret.iterator();
        while(iterator.hasNext()) {
            Tag tag = iterator.next();
            if(tag.attribute(name) != null) {
                if (!tag.attribute(name).equalsIgnoreCase(value)) {
                    iterator.remove();
                }
            }
            else
                iterator.remove();
        }
        return ret;
    }

    public Selection id(String id) {
        return attribute("id", id);
    }

    public Selection clazz(String clazz) {
        Selection ret = new Selection(this);
        Iterator<Tag> iterator = ret.iterator();
        while(iterator.hasNext()) {
            Tag tag = iterator.next();
            if(tag.attribute("class") != null) {
                if (!tag.attribute("class").toLowerCase().contains(clazz.toLowerCase())) {
                    iterator.remove();
                }
            } else
                iterator.remove();
        }
        return ret;
    }

    public Tag first () {
        if(size() == 0)
            return null;
        return get(0);
    }


    public static Selection create(ArrayList<Tag> tags) {
        return new Selection(tags);
    }
}
