import at.femoweb.html.HTMLReader;
import at.femoweb.html.Selection;
import at.femoweb.html.Tag;
import at.femoweb.html.TagType;
import static org.junit.Assert.*;

import org.junit.Test;

import java.net.URL;

/**
 * Created by felix on 10/22/14.
 */
public class TagTest {

    @Test
    public void testName() throws Exception {
        Tag tag = new Tag();
        tag.name("html");
        assertEquals("Tag Name", "html", tag.name());
    }

    @Test
    public void testType() throws Exception {
        Tag tag = new Tag();
        tag.type(TagType.FULL);
        assertEquals("Tag Type", TagType.FULL, tag.type());
    }

    @Test
    public void testAttributes() throws Exception {
        Tag tag = new Tag();
        tag.attribute("title", "Test");
        assertEquals("Tag Attribute", "Test", tag.attribute("title"));
    }

    @Test
    public void testRenderSimple() throws Exception {
        Tag tag = new Tag();
        tag.name("h1");
        tag.type(TagType.HALF);
        tag.add("Test");
        assertEquals("Simple Tag Render", "<h1>Test</h1>", tag.render());
    }

    @Test
    public void testRenderComplex() throws Exception {
        Tag root = new Tag();
        root.name("div");
        root.type(TagType.FULL);
        root.attribute("class", "container");
        root.add("Test");
        assertEquals("Complex Tag Render", "<div class=\"container\">\n\tTest\n</div>", root.render());
    }

    @Test
    public void testShortRender() throws Exception {
        Tag hr = new Tag();
        hr.name("hr");
        hr.type(TagType.SHORT);
        assertEquals("HR Render Text", "<hr />", hr.render());
    }

    @Test
    public void testNestedTags() throws Exception {
        Tag root = new Tag();
        root.name("div");
        root.type(TagType.FULL);
        root.attribute("class", "container");
        Tag heading = new Tag();
        heading.name("h1");
        heading.type(TagType.HALF);
        heading.add("Text");
        root.add(heading);
        assertEquals("Nested Tags Render", "<div class=\"container\">\n\t<h1>Text</h1>\n</div>", root.render());
    }

    @Test
    public void testRead() throws Exception {
        Tag root = HTMLReader.read("<div><h1 class=\"test\" >Hello</h1><hr />\n<xjs:page>This is a test<h2>Hello World</h2>This too\nAnd some other Text</xjs:page></div>");
        System.out.println(root.get(0));
        Tag div = root.get(0);
        assertEquals("Tag reading", "div", div.name());
        assertEquals("Reading Nested Tag", "h1", div.get(0).name());
        assertEquals("Reading Nested Tag Content", "Hello", div.get(0).get(0).render());
        assertEquals("Reading Nested short Tag", "hr", div.get(1).name());
        assertEquals("Reading Nested nest Tag", "xjs:page", div.get(2).name());
    }

    @Test
    public void testSelection() throws Exception {
        Tag root = HTMLReader.read("<div><h1>Hello</h1><hr />\n<xjs:page>This is a test<h2>Hello World</h2>This too\nAnd some other Text</xjs:page></div>");
        Tag div = root.get(0);
        Tag h1 = div.select().name("h1").first();
        assertEquals("Heading 1", "h1", h1.name());
    }

    @Test
    public void testOnline() throws Exception {
        Tag root = HTMLReader.read(new URL("http://www.codedread.com/testbed/pure-html.html"));
        Tag html = root.get(1);
        Tag div = html.select().name("body").first().select().name("p").first();
        System.out.println(html.render());
        assertEquals("Very complicated test", "p", div.name());
    }
}
