package at.femoweb.html;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by felix on 10/22/14.
 */
public class Tag {

    private String name;
    private TagType tagType = TagType.HALF;
    private HashMap<String, String> attributes;
    private ArrayList<Tag> children;
    private Tag parent;

    public Tag(Tag parent) {
        this();
        this.parent = parent;
        if(parent != null)
            parent.add(this);
    }

    public Tag(String name) {
        this();
        this.name = name;
    }

    public Tag(String name, Tag parent) {
        this(parent);
        this.name = name;
    }

    public Tag () {
        this.attributes = new HashMap<String, String>();
        this.children = new ArrayList<Tag>();
    }

    public String name() {
        return name;
    }

    public void name(String name) {
        this.name = name;
    }

    public TagType type() {
        return tagType;
    }

    public void type(TagType tagType) {
        this.tagType = tagType;
    }

    public void attribute(String name, String value) {
        attributes.put(name, value);
    }

    public String attribute(String name) {
        return attributes.get(name);
    }

    public String render() {
        return render(0, true);
    }

    public void add(String text) {
        TextTag textTag = new TextTag();
        textTag.text(text);
        children.add(textTag);
    }

    public void add(Tag tag) {
        children.add(tag);
    }

    public void add(Object object) {
        add(object.toString());
    }

    public Tag parent() {
        return parent;
    }

    public void parent(Tag parent) {
        this.parent = parent;
    }

    public Tag get(int index) {
        return children.get(index);
    }

    public int memoryId () {
        return System.identityHashCode(this);
    }

    public String render(int level, boolean alone) {
        if(name == null)
            return "";
        String ret = "";
        for(int i = 0; i < level; i++) {
            ret += "\t";
        }
        ret += "<" + name + " ";
        for(String attr : attributes.keySet()) {
            ret += attr + "=\"" + attributes.get(attr) + "\" ";
        }
        if(ret.endsWith(" ")) {
            ret = ret.substring(0, ret.length() - 1);
        }
        if(children.size() == 0) {
            switch (tagType) {
                case FULL:
                    ret += "></" + name + ">";
                    break;
                case HALF:
                case SHORT:
                    ret += " />";
                    break;
            }
        } else {
            switch (tagType) {
                case FULL:
                    ret += ">\n";
                    for(Tag tag : children) {
                        ret += tag.render(level + 1, false);
                    }
                    for(int i = 0; i < level; i++) {
                        ret += "\t";
                    }
                    ret += "</" + name + ">";
                    break;
                case HALF:
                    if(children.size() == 1) {
                        ret += ">" + children.get(0).render(0, true) + "</" + name + ">";
                    } else {
                        ret += ">\n";
                        for(Tag tag : children) {
                            ret += tag.render(level + 1, false);
                        }
                        for(int i = 0; i < level; i++) {
                            ret += "\t";
                        }
                        ret += "</" + name + ">";
                    }
                    break;
                case SHORT:
                    ret += " />";
                    break;
            }
        }
        if(!alone)
            ret += "\n";
        return ret;
    }

    public void replace(Tag tag) {
        replace(tag, true);
    }

    public void replace(Tag tag, boolean children) {
        int index = parent.children.indexOf(this);

        parent.children.set(index, tag);
        if(children)
            tag.add(this.children);
        tag.parent = parent;
    }

    public void add(Collection<Tag> collection) {
        for(Tag t : collection) {
            Tag t_ = t.copy(this);
            t_.parent(this);
            add(t_);
        }
    }

    private int hash;
    private boolean copy = false;

    protected Tag copy (Tag parent) {
        Tag tag = new Tag(name);
        tag.copy = true;
        tag.hash = hashCode();
        tag.type(type());
        tag.parent(parent);
        for(Tag t : children) {
            Tag c = t.copy(this);
            tag.add(c);
        }
        for(String name : attributes.keySet()) {
            tag.attribute(name, attribute(name));
        }
        return tag;
    }

    @Override
    public int hashCode() {
        return copy ? hash : super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return this.hashCode() == o.hashCode();
    }

    public Selection select() {
        return Selection.create(children);
    }

    public Selection find(String query) {
        Selection selection = Selection.create(children);
        String search = "";
        int type = -1;
        for(char c : query.toCharArray()) {
            if(c == '.') {
                type = 1;
                search = "";
                find(search, type, selection);
            } else if (c == '#') {
                type = 2;
                search = "";
                find(search, type, selection);
            } else if (c == ' ') {
                type = 3;
                find(search, type, selection);
                search = "";
            } else if (c == '+') {
                type = 4;
                find(search, type, selection);
                search = "";
            } else {
                search += c;
            }
        }
        find(search, type, selection);
        return selection;
    }

    private Selection find(String search, int type, Selection selection) {
        if(type == -1 || type == 1) {
            return selection.name(search);
        }
        return selection;
    }

    public ArrayList<Tag> children() {
        return children;
    }

    @Override
    public String toString() {
        return render();
    }

    public String text() {
        String ret = "";
        for(Tag t : children) {
            ret += t;
        }
        return ret;
    }
}
