package at.femoweb.html;

/**
 * Created by felix on 10/22/14.
 */
public class TextTag extends Tag {

    private String text;

    public void text(String text) {
        this.text = text;
    }

    public String text() {
        return text;
    }

    @Override
    protected String render(int level, boolean alone) {
        String ret = "";
        for(int i = 0; i < level; i++) {
            ret += "\t";
        }
        ret += text;
        if(!alone)
            ret += "\n";
        return ret;
    }

    @Override
    protected Tag copy(Tag parent) {
        TextTag textTag = new TextTag();
        textTag.text(text);
        textTag.parent(parent);
        return textTag;
    }
}
