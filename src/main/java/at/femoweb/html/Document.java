package at.femoweb.html;

/**
 * Created by felix on 10/22/14.
 */
public class Document {

    private String doctype;
    private Tag root;
    private Tag content;
    private Tag body;
    private Tag head;

    public static Document readDocument(String document) {
        Document doc = new Document();
        doc.root = HTMLReader.read(document);
        doc.doctype = doc.root.get(0).text();
        doc.content = doc.root.get(1);
        return doc;
    }

    private Document () {}

    public Document(String doctype) {
        doctype(doctype);
        if(doctype.equalsIgnoreCase("html")) {
            content = new Tag("html");
            head = new Tag("head", content);
            body = new Tag("body", content);
        }
    }

    public String doctype() {
        return doctype;
    }

    public void doctype(String doctype) {
        this.doctype = doctype;
    }

    public Tag content() {
        return content;
    }

    public void content(Tag content) {
        this.content = content;
    }

    public String render () {
        String ret = "";
        ret += "<!DOCTYPE " + doctype + ">\n";
        return ret + content.render();
    }

    public Tag head() {
        return head;
    }

    public void head(Tag head) {
        this.head = head;
    }

    public Tag body() {
        return body;
    }

    public void body(Tag body) {
        this.body = body;
    }
}
