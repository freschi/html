package at.femoweb.html;

import javax.print.Doc;
import java.io.*;
import java.lang.management.BufferPoolMXBean;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by felix on 10/22/14.
 */
public class HTMLReader {

    public static Document open(InputStream inputStream) throws IOException {
        String text = "", line;
        BufferedReader din = new BufferedReader(new InputStreamReader(inputStream));
        while((line = din.readLine()) != null) {
            text += line + "\n";
        }
        return Document.readDocument(text);
    }

    public static Document open(File file) throws IOException {
        return open(new FileInputStream(file));
    }

    public static Document open(URL url) throws IOException {
        return open(url.openStream());
    }

    public static Document open(URI uri) throws IOException {
        return open(uri.toURL());
    }

    public static Tag read(InputStream inputStream) throws IOException {
        String text = "", line;
        BufferedReader din = new BufferedReader(new InputStreamReader(inputStream));
        while((line = din.readLine()) != null) {
            text += line + "\n";
        }
        return read(text);
    }

    public static Tag read(File file) throws IOException {
        return read(new FileInputStream(file));
    }

    public static Tag read(URL url) throws IOException {
        return read(url.openStream());
    }

    public static Tag read(URI uri) throws IOException {
        return read(uri.toURL());
    }

    public static Tag read(String document) {
        Tag root = new Tag();
        document = document.replace("\t", "");
        Tag current = root;
        String currentName = "";
        int type = 0;
        boolean doctype = false;
        boolean inQuote = false;
        for(int i = 0; i < document.length(); i++) {
            char c = document.charAt(i);
            if(c == '\t')
                continue;
            if(c == '<') {
                if (currentName.equals("\t")) {
                    currentName = "";
                } else if(!currentName.equals("")) {
                    if(!currentName.trim().equals(""))
                        current.add(currentName);
                    currentName = "";
                }
                if(document.charAt(i + 1) == '!') {
                    doctype = true;
                }
                type = 1;
            } else if (c == '>' && !currentName.equals("/")) {
                if(currentName.endsWith("/")) {
                    current.name(currentName.substring(0, currentName.length() - 1).trim());
                    current.type(TagType.SHORT);
                    current = current.parent();
                } else if (currentName.startsWith("/")) {
                    if (currentName.substring(1).equalsIgnoreCase(current.name())) {
                        current.type(TagType.HALF);
                        current = current.parent();
                    } else {
                        return null;
                    }
                } else if (type == 2) {
                    if(currentName.contains("=")) {
                        String name, value;
                        name = currentName.substring(0, currentName.indexOf("="));
                        value = currentName.substring(currentName.indexOf("=") + 1);
                        if(value.startsWith("\"")) {
                            value = value.substring(1);
                        }
                        if(value.endsWith("\"")) {
                            value = value.substring(0, value.length() - 1);
                        }
                        current.attribute(name, value);
                    } else if (doctype) {
                        current.add(currentName);
                        current = current.parent();
                        doctype = false;
                    }
                } else {
                    current = new Tag(current);
                    current.name(currentName.trim());
                }
                currentName = "";
                type = 0;
            } else if (c == '>' && currentName.equals("/")) {
                currentName = "";
                current = current.parent();
            } else if (c == '\n') {
                if(!currentName.trim().equals("")) {
                    current.add(currentName);
                    currentName = "";
                }
            } else {
                if(type == 1 && c == ' ' && !inQuote) {
                    current = new Tag(current);
                    current.name(currentName.trim());
                    currentName = "";
                    type = 2;
                } else if(type == 2 && c == ' ' && currentName.contains("=") && !inQuote) {
                    String name, value;
                    name = currentName.substring(0, currentName.indexOf("="));
                    value = currentName.substring(currentName.indexOf("=") + 1);
                    if(value.startsWith("\"")) {
                        value = value.substring(1);
                    }
                    if(value.endsWith("\"")) {
                        value = value.substring(0, value.length() - 1);
                    }
                    current.attribute(name, value);
                    currentName = "";
                } else if(type == 2 && c == ' ') {
                    currentName = "";
                } else if (type == 1 && c == '\"') {
                    inQuote = !inQuote;
                } else {
                    currentName += c;
                }
            }

        }
        return root;
    }
}
